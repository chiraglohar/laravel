@extends('app')
@section('section')
<div class="card">
    <div class="cart-title">
        Event
    </div>
    <div class="card-body">
      <p>Event Title : {{ $event->title }}</p>
      <p>Event Occurence :</p>
      @php
      list($recurrence,$occurence) = explode('-',$event->recurrence);
      @endphp
      
      <table class="table">
        @php
        $date1 = \Carbon\Carbon::parse($event->start_date);
        $date2 = \Carbon\Carbon::parse($event->end_date);
        @endphp
         <tr>
            <td>Date</td>
            <td>Day Name</td>
        </tr>
        @php $i=0; @endphp
        @while($date1->lte($date2))
        @php
         $i++
        @endphp
        <tr>
            <td>{{ \Carbon\Carbon::parse($date1)->format('Y-m-d') }}</td>
            <td>{{ \Carbon\Carbon::parse($date1)->format('l') }}</td>
        </tr>
        @php
        $day = 1;
        if($recurrence == 'Every'){
            $day = 1;
        } elseif($recurrence == 'Every Other'){
            $day = 2;
        } elseif($recurrence == 'Every Third'){
            $day = 3;
        } elseif($recurrence == 'Every Fourth'){
            $day = 4;
        }
        if($occurence == 'Day'){
            $date1 = \Carbon\Carbon::parse($date1)->addDays($day);
        } elseif($occurence == 'Week'){
            $day = $day*7;
            $date1 =  \Carbon\Carbon::parse($date1)->addDays($day);
        } elseif($occurence == 'Month'){
         
            $date1 = \Carbon\Carbon::parse($date1)->addMonth($day);
        } elseif($occurence == 'Year'){
            $date1 =  \Carbon\Carbon::parse($date1)->addYears($day);
        }
        
        
        @endphp
        @endwhile
      </table>
      Total Reccurence Event -: {{ $i }}
    </div>
</div>
@endsection