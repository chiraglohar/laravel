@extends('app')
@section('section')
<div class="card">
    <div class="cart-title">
         <a href="{{ url('event/create')}}" class="btn btn-success pull-right">Add Event</a>
    </div>
    <div class="card-body">
        <table class="table">
            <tr>
                <td>Sr No.</td>
                <td>Title</td>
                <td>Dates</td>
                <td>Occurrence</td>
                <td>Action</td>
            </tr>
            @foreach($events as $i=> $event)
            <tr>
                <th>{{ $i+1 }}</th>
                <th>{{ $event->title }}</th>
                <th>{{ \Carbon\Carbon::parse($event->start_date)->format('d M,y') }} TO {{ \Carbon\Carbon::parse($event->end_date)->format('d M,y') }}</th>
                <th>{{ $event->recurrence }}</th>
                <th>
                    <a href="{{ url('event/'.$event->id) }}" class="btn btn-primary">View</a>
                    <a href="{{url('event/'.$event->id.'/edit') }}" class="btn btn-success">Edit</a>
                    <a href="{{ url('event/delete/'.$event->id) }}" class="btn btn-danger">Delete</a>
                </th>
            </tr>
            @endforeach
        </table>
    </div>
</div>
@endsection