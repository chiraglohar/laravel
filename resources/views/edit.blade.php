@extends('app')
@section('section')
<div class="card">
    <div class="cart-title">
        Event
    </div>
    <div class="card-body">
        <form method="post" action="{{ url('event/'.$event->id) }}">
           
            @csrf()
            <input type="hidden" name="_method" value="PUT">
            <div class="mb-3">
              <label for="exampleInputEmail1" class="form-label">Title</label>
              <input type="text" required class="form-control" name="title"  value="{{ old('title',$event->title) }}">
            </div>
            <div class="mb-3">
                <label for="exampleInputEmail1" class="form-label">Description</label>
                <input type="text" required class="form-control" name="description"  value="{{ old('description',$event->description) }}">
            </div>
            <div class="mb-3">
               <div class="row">
                   <div class="col">
                        <label for="exampleInputEmail1" class="form-label">Start Date</label>
                        <input type="date" required class="form-control" name="start_date"  value="{{ old('start_date',\Carbon\Carbon::parse($event->start_date)->format('Y-m-d') ) }}">
                   </div>
                   <div class="col">
                        <label for="exampleInputEmail1" class="form-label">End Date</label>
                        <input type="date" required class="form-control" name="end_date"  value="{{ old('start_date',\Carbon\Carbon::parse($event->end_date)->format('Y-m-d') ) }}">
                    </div>
               </div>
            </div>
            @php
            list($recurrence,$occurence) = explode('-',$event->recurrence);
            @endphp
            <div class="mb-3">
                <label for="exampleInputEmail1" class="form-label">Reccurence</label>
                <div class="row">
                    <div class="col">
                        <select name="recurrence" required id="" class="form-control">
                            <option value="Every" {{ $recurrence == 'Every' ?'selected':'' }}>Every</option>
                            <option value="Every Other"  {{ $recurrence == 'Every Other' ?'selected':'' }}>Every Other</option>
                            <option value="Every Third"  {{ $recurrence == 'Every Third' ?'selected':'' }}>Every Third</option>
                            <option value="Every Fourth"  {{ $recurrence == 'Every Fourth' ?'selected':'' }}>Every Fourth</option>
                        </select>
                    </div>
                    <div class="col">
                        <select name="occurence" required id="" class="form-control">
                            <option value="Day" {{ $occurence == 'Day' ?'selected':'' }}> Day</option>
                            <option value="Week" {{ $occurence == 'Week' ?'selected':'' }}> Week</option>
                            <option value="Month" {{ $occurence == 'Month' ?'selected':'' }}> Month</option>
                            <option value="Year" {{ $occurence == 'Year' ?'selected':'' }}>Year</option>
                        </select>
                   </div>
                   
                </div>
             </div>
            <button type="submit" class="btn btn-primary">Submit</button>
          </form>
    </div>
</div>
@endsection