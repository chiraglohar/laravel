@extends('app')
@section('section')
<div class="card">
    <div class="cart-title">
        Event
    </div>
    <div class="card-body">
        <form method="post" action="{{ url('event') }}">
            @csrf()
            <div class="mb-3">
              <label for="exampleInputEmail1" class="form-label">Title</label>
              <input type="text" required class="form-control" name="title"  value="{{ old('title') }}">
            </div>
            <div class="mb-3">
                <label for="exampleInputEmail1" class="form-label">Description</label>
                <input type="text" required class="form-control" name="description"  value="{{ old('description') }}">
            </div>
            <div class="mb-3">
               <div class="row">
                   <div class="col">
                        <label for="exampleInputEmail1" class="form-label">Start Date</label>
                        <input type="date" required class="form-control" name="start_date"  value="{{ old('start_date') }}">
                   </div>
                   <div class="col">
                        <label for="exampleInputEmail1" class="form-label">End Date</label>
                        <input type="date"  required class="form-control" name="end_date"  value="{{ old('end_date') }}">
                    </div>
               </div>
            </div>
            <div class="mb-3">
                <label for="exampleInputEmail1" class="form-label">Reccurence</label>
                <div class="row">
                    <div class="col">
                        <select name="recurrence" id="" class="form-control">
                            <option value="Every">Every</option>
                            <option value="Every Other">Every Other</option>
                            <option value="Every Third">Every Third</option>
                            <option value="Every Fourth">Every Fourth</option>
                        </select>
                    </div>
                    <div class="col">
                        <select name="occurence" required id="" class="form-control">
                            <option value="Day"> Day</option>
                            <option value="Week"> Week</option>
                            <option value="Month"> Month</option>
                            <option value="Year">Year</option>
                        </select>
                   </div>
                   
                </div>
             </div>
            <button type="submit" class="btn btn-primary">Submit</button>
          </form>
    </div>
</div>
@endsection