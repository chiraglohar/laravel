<?php

namespace App\Http\Controllers;

use App\Models\Event;
use Illuminate\Http\Request;
use Carbon\Carbon;
class EventController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $events  = Event::get();
       
        return view('index',compact('events'));
    }
    
    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        return view('add');
    }
    
    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        $request->validate([
            'title'=>'required',
            'description'=>'required',
            'start_date'=>'required',
            'end_date'=>'required',
            'recurrence'=>'required',
            'recurrence'=>'required',
            'occurence'=>'required'
        ]);
        $event = new Event;
        $event->title = $request->title;
        $event->description = $request->description;
        $event->start_date = $request->start_date;
        $event->end_date = $request->end_date;
        $event->recurrence = $request->recurrence .'-'.  $request->occurence;
        $event->save();
        return redirect('event')->with(['success'=>'Added succesfully']);
    }
    
    /**
    * Display the specified resource.
    *
    * @param  \App\Models\Event  $event
    * @return \Illuminate\Http\Response
    */
    public function show(Event $event)
    {
        return view('view',compact('event'));
    }
    
    /**
    * Show the form for editing the specified resource.
    *
    * @param  \App\Models\Event  $event
    * @return \Illuminate\Http\Response
    */
    public function edit(Event $event)
    {
        return view('edit',compact('event'));
    }
    
    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  \App\Models\Event  $event
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, Event $event)
    {
        $request->validate([
            'title'=>'required',
            'description'=>'required',
            'start_date'=>'required',
            'end_date'=>'required',
            'recurrence'=>'required',
            'recurrence'=>'required',
            'occurence'=>'required'
        ]);
        $event->title = $request->title;
        $event->description = $request->description;
        $event->start_date = $request->start_date;
        $event->end_date = $request->end_date;
        $event->recurrence = $request->recurrence .'-'.  $request->occurence;
        $event->save();
        return redirect('event')->with(['success'=>'Update succesfully']);
    }
    
    /**
    * Remove the specified resource from storage.
    *
    * @param  \App\Models\Event  $event
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
         Event::destroy($id);
        return redirect('event')->with(['success'=>'Delete succesfully']);
    }
}
